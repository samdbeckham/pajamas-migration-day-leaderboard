import gql from "graphql-tag";

const migrationIssuesQuery = gql`
  query migrationIssues {
    project(fullPath: "gitlab-org/gitlab") {
      issues(
        labelName: "Pajamas Migration Day"
        or: {
          labelNames: [
            "component:button"
            "component:dropdown"
            "component:dropdown-collapsible-listbox"
            "component:dropdown-combobox"
            "component:dropdown-disclosure"
          ]
        }
        state: closed
        closedAfter: "2023-10-11"
        assigneeWildcardId: ANY
      ) {
        edges {
          node {
            id
            webUrl
            assignees {
              edges {
                node {
                  id
                  username
                  name
                  avatarUrl
                }
              }
            }
          }
        }
      }
    }
  }
`;

export default migrationIssuesQuery;
