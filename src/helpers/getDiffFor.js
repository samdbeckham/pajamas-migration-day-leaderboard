const getDiffFor = (a, b) => {
  return a.filter((x) => !b.includes(x));
};
export default getDiffFor;
